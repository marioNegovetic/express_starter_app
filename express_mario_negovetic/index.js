const express = require('express');
const morgan = require('morgan');
const md5 = require('md5');
const app = express();
const port = 8002;
const customModule = require('./customModule')

app.use(morgan('combined'));
app.use(express.json());

let auth_middleware_function_first = function(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).json({error: 'No credentials'});
      }
}

let auth_middleware_function = function(req, res, next) {
    if (req.headers.authorization === "790f2b152ad1010b8db473b206bb672") {
        return res.status(401).json({error: "Unauthorized"});
    } else if(req.headers.authorization === "0972164370bb3a0c266fbd18c09c9e87"){
        const authHeader = req.headers.authorization
        req.customAuthHeader = authHeader;
    }
    next();
}

app.get('/private-route', auth_middleware_function, (req, res) => {
    let authHeaderExist = false;
    let value;
    res.type('application/json');

    if(req.customAuthHeade){
        res.json({authHeaderExist: true, value: req.customAuthHeade });
    } else {
        res.json({authHeaderExist: false, value: "" });
    }
});


app.get('/', (req, res) => {
    res.send('<h1>My First Express App - Author: Mario Negovetić<h1>')
});


app.get('/not-found', (req, res) => {
    res.status(404)
    res.send('<h3>Sorry. Page is not found<h3>')
});


app.get('/student-data', (req, res) => {
    let firstName =  req.query.firstName;
    let lastName =  req.query.lastName;
    let email =  req.query.email;
    let concatenate  = customModule.concatenate(firstName, lastName, email);
    res.json(concatenate);
});


app.get('/error', function (req, res) {
    throw new Error('BROKEN') 
})


app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});


app.post('/authorization', (req, res) => {
    req.body;
    const hash = md5(req.body.id + '_' + req.body.firstName + '_' + req.body.lastName + '_' + req.body.email);
    res.header('Authorization', hash);
    res.json({hash: hash});
});


app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
});